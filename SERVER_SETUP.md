# Server Setup

## Packages

```bash
sudo apt-get install \
  python3-dev \
  libcups2-dev \
  libssl-dev \
  python3-virtualenv \
  git
```

## Allow Port 80

```
sudo setcap 'cap_net_bind_service=+ep' $HOME/print_server/start_gunicorn
```
(this is only necessary automatic deployment using ansible is not used)

## Config printer
Install as LPD.
