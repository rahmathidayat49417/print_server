#!/bin/bash
set -x

ansible-playbook --ask-sudo-pass -i inventory/ipho2018-test deploy.yml $*
