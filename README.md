# ExamTools Print Server

## Deploy

Ansible scripts are provided for a local installation which configures the following services:
* **Gunicorn** running the app (multiple processes)
* **Nginx** proxying the requests to the app

```bash
ansible-playbook --ask-pass -i inventory/XYZ deploy.yml
```

## How test using CURL

```bash
curl  -X POST -H "Content-Type: multipart/form-data"  -H "Authorization: IPhOToken secretAPIKey" -F "file=@T2-Q_Theory_2_Question.pdf" http://127.0.0.1:5000/print/print-queue-name
```

## Connect to Server via SSH tunnel

```
autossh -M 20002 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -o "ExitOnForwardFailure yes" -R 60006:localhost:80 printserver@apho2018.oly-exams.org
```

Use ``localhost:60006`` as printer host in the exam tools deployment configuration (vars.yml).
