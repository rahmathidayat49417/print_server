import cups

def print_document(queue, fname, opts={}, title='IPhO Print', add_banner_page=False):
    default_opts = {
        'ColourModel': 'Colour',
        'Duplex': 'None',
        # 'Staple': '1PLU',
        'fit-to-page': 'True',
    }
    default_opts.update(opts)
    if add_banner_page:
        default_opts.update({'job-sheets': 'standard'})

    conn = cups.Connection ()
    conn.printFile(queue, fname, title, default_opts)
